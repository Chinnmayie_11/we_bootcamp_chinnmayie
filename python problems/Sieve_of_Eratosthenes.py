def sieve_of_eratosthenes(start: int, end: int) -> list:
    primes = [True] * (end - start + 1)
    primes[0], primes[1] = False, False

    for num in range(2, int(end**0.5) + 1):
        if primes[num]:
            for multiple in range(num*num, end + 1, num):
                if multiple >= start:
                    primes[multiple - start] = False

    return [num + start for num in range(len(primes)) if primes[num]]
ok = sieve_of_eratosthenes(100,1000)
print(ok)