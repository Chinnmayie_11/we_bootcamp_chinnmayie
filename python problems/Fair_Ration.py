def fair_rations(persons : list) -> int:
    loaf = 0
    for i in range(len(persons)-1):
        if persons[i] % 2 != 0:
            persons[i] += 1
            persons[i + 1] += 1
            loaf += 2
        else:
            continue
    return loaf


print(fair_rations([1, 3, 5 ,6, 7]))  #Output should be 6
print(fair_rations([4, 5, 6, 7]))  # Output should be 4
print(fair_rations([2, 2, 2, 2]))  # Output should be 0



