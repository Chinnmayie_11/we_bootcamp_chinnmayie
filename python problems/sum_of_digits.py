def digits_sum(n:int) -> int:
    d = n % 9
    return 9 if d == 0 else d
    #return 1 + (n-1) % 9

print(digits_sum(6492846))