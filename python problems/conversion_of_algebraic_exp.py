SPACE = ' '

def clean(polynomial):
    return ''.join(ch for ch in polynomial if ch != SPACE)
def terms(polynomial):
    return clean(polynomial).replace('-','$-').replace('+','$').split("$")
def new(polynomial):
    return '+'.join(reversed(terms(polynomial))).replace('+-','-')
print(new("2x^4 + 3x^3 - 4x^2 - 2x +7"))
