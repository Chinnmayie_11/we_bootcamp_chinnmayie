import sys

student_data = {
    'A': [12, 14, 16],
    'B': [5, 6, 7],
    'C': [17, 20, 23],
    'D': [2, 40, 12],
    'E': [3, 41, 13],
    'F': [7, 8, 9],
    'G': [4, 5, 6]
}

def comparing_the_lists(student_1: list, student_2: list) -> str:
    for i in range(len(student_2)):
        if student_1[i] > student_2[i]:
            return ">"
        elif student_1[i] < student_2[i]:
            return "<"
    return "="

# Helper function to find transitive relations
def is_transitive(relation, a, b, c):
    return (a, b) in relation and (b, c) in relation

relation = {}
for student_1 in student_data:
    for student_2 in student_data:
        if student_1 != student_2:
            comparison = comparing_the_lists(student_data[student_1], student_data[student_2])
            if comparison != "=":
                relation[(student_1, student_2)] = comparison

printed = set()
for (a, b), ab_rel in relation.items():
    for (b2, c), bc_rel in relation.items():
        if b == b2 and ab_rel == bc_rel:
            printed.add((a, c, ab_rel))

for (a, b), ab_rel in relation.items():
    if (a, b, ab_rel) not in printed:
        print(f"{a} {ab_rel} {b}")
