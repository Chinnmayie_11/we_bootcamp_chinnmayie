def new_code(number : str):
    number = [int(x) for x in str(number)[::1]]
    for (i) in range(1,12,2):
        number[i] *= 3
    #for (j) in range(1,12,2):
        #number[j] *= 3
    return number[::1]


def sum_code(number):
    return sum(new_code(number))

def check_digit(number : int ) -> bool:
    return (10 - (sum_code(number)) % 10)

print(check_digit(978030640615))